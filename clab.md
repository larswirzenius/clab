# Introduction

`clab` is a command line address book application. It has no
interactive features. This document collects its acceptance criteria.

# Empty database

~~~scenario
given an installed clab
when I run clab lint
then command is successful

when I run clab search Alice
then command is successful
then stdout is exactly ""

when I try to run clab mutt-query Alice
then command fails
~~~

# Alice and Bob

Next, let's add records for Alice and Bob, and make
sure searches find only the right records.


~~~scenario
given an installed clab

given file .local/share/clab/address-book.yaml from address-book.yaml

when I run clab lint
then command is successful

when I run clab list
then command is successful
then stdout is valid YAML
then stdout contains "Alice Atherthon"
then stdout contains "Bob Bobbington"

when I run clab search Alice
then command is successful
then stdout is valid YAML
then stdout contains "Alice Atherthon"
then stdout doesn't contain "Bob"

when I run clab mutt-query Alice
then command is successful
then stdout is exactly "clab found matches:\nalice@example.com\tAlice Atherthon\n"
~~~

~~~{#address-book.yaml .file .yaml}
- name: Alice Atherthon
  email:
    work: alice@example.com
  tags: [co-worker]
  last_checked: 2021-09
- name: Bob Bobbington
  email:
    personal: bob@example.com
  last_checked: 2021-09
~~~

~~~{#address-book-reformatted .file .yaml}
---
- name: Alice Atherthon
  email:
    work: alice@example.com
  tags:
    - co-worker
  last_checked: 2021-09
- name: Bob Bobbington
  email:
    personal: bob@example.com
  last_checked: 2021-09
~~~


# Search by tag

This scenario verifies that `clab` can find entries based on tags.

~~~scenario
given an installed clab
given file .local/share/clab/address-book.yaml from address-book.yaml

when I run clab tagged co-worker
then command is successful
then stdout contains "Alice Atherthon"
then stdout doesn't contain "Bob"
~~~

# Full text search

This scenario verifies that `clab` does a full text search.

~~~scenario
given an installed clab
given file .local/share/clab/address-book.yaml from address-book.yaml

when I run clab search example
then command is successful
then stdout contains "Alice Atherthon"
then stdout contains "Bob"

when I run clab search email
then command is successful
then stdout contains "Alice Atherthon"
then stdout contains "Bob"
~~~

# Reformat address book

This scenario verifies that `clab` can reformat the address book in
place.

~~~scenario
given an installed clab
given file .local/share/clab/address-book.yaml from address-book.yaml
given file reformatted.yaml from address-book-reformatted

when I run clab reformat
then command is successful
then files .local/share/clab/address-book.yaml and reformatted.yaml match
~~~
