import io
import os
import yaml


def install_clab(ctx):
    runcmd_prepend_to_path = globals()["runcmd_prepend_to_path"]
    srcdir = globals()["srcdir"]

    # Add the directory with built Rust binaries to the path.
    default_target = os.path.join(srcdir, "target")
    target = os.environ.get("CARGO_TARGET_DIR", default_target)
    runcmd_prepend_to_path(ctx, dirname=os.path.join(target, "debug"))
    ctx["server-binary"] = os.path.join(target, "debug", "obnam-server")


def stdout_is_yaml(ctx):
    runcmd_get_stdout = globals()["runcmd_get_stdout"]
    stdout = runcmd_get_stdout(ctx)
    yaml.safe_load(io.StringIO(stdout))
